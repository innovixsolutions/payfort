using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Routing;
using Microsoft.AspNet.FriendlyUrls;

namespace PayFortIntegration
{
    public static class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.Ignore("");

            RouteValueDictionary defaultLocale = new RouteValueDictionary { { "locale", "en" } };
            RouteValueDictionary localeConstraints = new RouteValueDictionary { { "locale", "en|ar" } };

            //Review Page
            //RouteTable.Routes.MapPageRoute("RoutaPage", "RoutaPage/{r}", "~/RoutaPage.aspx", false, defaultLocale,
            // localeConstraints);

            var settings = new FriendlyUrlSettings();
            
            settings.AutoRedirectMode = RedirectMode.Permanent;
            routes.EnableFriendlyUrls(settings);
        }
    }
}
