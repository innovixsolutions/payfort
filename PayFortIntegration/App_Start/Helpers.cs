﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PayFortIntegration
{
    public static  class Helpers
    {
        public static string GetCurrentHostName()
        {
            var request = HttpContext.Current.Request;
            var port = (request.Url.IsDefaultPort ? "" : ":" + request.Url.Port);
            string currentHostUrl = $"{request.Url.Scheme}{Uri.SchemeDelimiter}{request.Url.Host}{port}";

            return currentHostUrl;
        }
    }
}