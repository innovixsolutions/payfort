﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PayFortIntegration
{
    public partial class FormSubmission : System.Web.UI.Page
    {
        public string OrderReference { get; set; }
        public string ShaSignature { get; set; }

        protected void Page_Init(object sender, EventArgs e)
        {
            OrderReference = new Random(DateTime.Now.Millisecond).Next(111111111, 999999999).ToString();
            ShaSignature = CreateDictionaryFromClass();
        }
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public string CreateDictionaryFromClass()
        {
            SortedList<string, string> h = new SortedList<string, string>();
            h.Add("service_command", "TOKENIZATION");
            h.Add("access_code", "TBuulxmCCpvcLLkDqrrV");
            h.Add("merchant_identifier", "CwiRclrh");
            h.Add("merchant_reference", OrderReference);
            h.Add("return_url", "https://localhost:44312/order-result");
            h.Add("language", "en");

            return PrepareForSignature(h);
        }



        public string PrepareForSignature(SortedList<string, string> dic)
        {
            StringBuilder Signature = new StringBuilder();
            Signature.Append("TESTSHAIN");
            foreach (var item in dic)
            {
                Signature.Append(item.Key);
                Signature.Append("=");
                Signature.Append(item.Value);
            }
            Signature.Append("TESTSHAIN");

            return CreateSignatureSha256(Signature.ToString());
        }



        static string CreateSignatureSha256(string data)
        {

            using (SHA256 sha256Hash = SHA256.Create())
            {

                byte[] bytes = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(data));

                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < bytes.Length; i++)
                {
                    builder.Append(bytes[i].ToString("x2"));
                }
                return builder.ToString();
            }
        }
    }
}