﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RoutePage.aspx.cs" Inherits="PayFortIntegration.RoutePage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form>
        <section class="nav">
            <ul>
                <li class="lead">Payment Method</li>
                <li class="lead">Pay</li>
                <li class="active lead">Done</li>
            </ul>
        </section>
        <section class="confirmation">
            <label class="success" for="">Success</label>
            <!-- <label class="failed" for="" >Failed</label> -->
            <small>Thank You For Your Order</small>
        </section>
        <section class="order-confirmation">
            <label for="" class="lead">Order ID : 'fort_id'</label>
        </section>
        <div class="h-seperator"></div>
        <section class="details">
            <h3>Response Details</h3>
            <br />
            <table>
                <tr>
                    <th>Parameter Name
                    </th>
                    <th>Parameter Value
                    </th>
                </tr>


                <% foreach (var item in Inputs.AllKeys)
                    {  %>
                <tr>
                    <td>
                        <%= item.ToString() %>
                    </td>

                    <td>
                        <%= Inputs.GetValues(item.ToString()).FirstOrDefault() %>
                    </td>

                </tr>
                <%
                    }
                %>
            </table>
        </section>

    </form>

</body>
</html>
