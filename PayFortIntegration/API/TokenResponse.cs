﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using PayFortIntegration.Model;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;
using System.Web.UI.WebControls;

using PaymentProcessor.PayFort.Model;
using PaymentProcessor.PayFort;
using System.Configuration;
using PaymentProcessor.PayFort.Managers;

namespace PayFortIntegration
{
    public class TokenResponseController : ApiController
    {

        [HttpPost]
        public void GetTokenResponse()
        {
            var payFortManager = new PayFortManager(Convert.ToBoolean(ConfigurationManager.AppSettings["UseTestmode"]));
            payFortManager.ProcessTokenResponse();
        }

    }
}