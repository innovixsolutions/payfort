﻿using Newtonsoft.Json;
using PayFortIntegration.Model;
using PaymentProcessor.PayFort;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http;
using PaymentProcessor.PayFort.Infrastructure;
using PaymentProcessor.PayFort.Managers;
using System.Configuration;

namespace PayFortIntegration
{
    public class PurchaseResponseController : ApiController
    {
        [HttpPost]
        public void GetPurchaseResponse()
        {
            var payFortManager = new PayFortManager(Convert.ToBoolean(ConfigurationManager.AppSettings["UseTestmode"]));
            payFortManager.ProcessPurchaseResponse();
        }
    }
}