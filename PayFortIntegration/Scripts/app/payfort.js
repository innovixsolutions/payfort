﻿function PayFortManager() {

    this.submitTokenRequest = function (tokenUrl) { //response will be sent over by PayFort Server to the "return_url" value

        $('<form>').attr({
            style: 'display:none',
            name: 'payfort_payment_form',
            id: 'payfort_payment_form',
            action: tokenUrl,
            method: "post"
        }).appendTo('body');

        var mp2_params = {};
        var expDate = $('#payfort_fort_mp2_expiry_year').val() + $('#payfort_fort_mp2_expiry_month').val();
        mp2_params.card_holder_name = $('#payfort_fort_mp2_card_holder_name').val();
        mp2_params.card_number = $('#payfort_fort_mp2_card_number').val();
        mp2_params.expiry_date = expDate;
        mp2_params.card_security_code = $('#payfort_fort_mp2_cvv').val();
        $.each(mp2_params, function (name, value) {
            $('<input>').attr({
                type: 'hidden',
                id: name,
                name: name,
                value: value
            }).appendTo('#payfort_payment_merchant_page2');


        });
        $('#payfort_payment_merchant_page2').appendTo('#payfort_payment_form');

        $('<input>').attr({
            type: 'submit',
            id: 'submit',
        }).appendTo('#payfort_payment_form');

        $('#payfort_payment_form input[type = submit]').click();
    };

}


