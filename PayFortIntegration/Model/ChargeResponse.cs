﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PayFortIntegration.Model
{
    [JsonObject(MemberSerialization.OptIn)]
    public class ChargeResponse
    {
        [JsonProperty("command")]
        public string command { get; set; }

        [JsonProperty("access_code")]
        public string access_code { get; set; }

        [JsonProperty("merchant_identifier")]
        public string merchant_identifier { get; set; }

        [JsonProperty("merchant_reference")]
        public string merchant_reference { get; set; }

        [JsonProperty("amount")]
        public string amount { get; set; }

        [JsonProperty("currency")]
        public string currency { get; set; }

        [JsonProperty("language")]
        public string language { get; set; }

        [JsonProperty("customer_email")]
        public string customer_email { get; set; }

        [JsonProperty("token_name")]
        public string token_name { get; set; }

        [JsonProperty("signature")]
        public string signature { get; set; }

        [JsonProperty("fort_id")]
        public string fort_id { get; set; }

        [JsonProperty("payment_option")]
        public string payment_option { get; set; }

        [JsonProperty("response_code")]
        public string response_code { get; set; }

        [JsonProperty("response_message")]
        public string response_message { get; set; }

        [JsonProperty("order_description")]
        public string order_description { get; set; }

        [JsonProperty("status")]
        public string status { get; set; }

        [JsonProperty("settlement_reference")]
        public string settlement_reference { get; set; }

        [JsonProperty("3ds_url")]
        public string dsurl  { get; set; }
}
}