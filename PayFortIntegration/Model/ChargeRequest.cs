﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PayFortIntegration.Model
{
    public class ChargeRequest
    {

        [JsonProperty("command")]
        public string Command { get; set; }

        [JsonProperty("access_code")]
        public string AccessCode { get; set; }

        [JsonProperty("merchant_identifier")]
        public string MerchantIdentifier { get; set; }

        [JsonProperty("merchant_reference")]
        public string MerchantReference { get; set; }

        [JsonProperty("amount")]
        public string Amount { get; set; }

        [JsonProperty("currency")]
        public string Currency { get; set; }

        [JsonProperty("language")]
        public string Language { get; set; }

        [JsonProperty("token_name")]
        public string TokenName { get; set; }

        [JsonProperty("signature")]
        public string Signature { get; set; }

        [JsonProperty("return_url")]
        public string ReturnUrl { get; set; }

        [JsonProperty("customer_email")]
        public string CustomerEmail { get; set; }

        //[JsonProperty("customer_ip")]
        //public string CustomerIp { get; set; }

        [JsonProperty("order_description")]
        public string OrderDescription { get; set; }

        //[JsonProperty("card_number")]
        //public string CardNumber { get; set; }

        //[JsonProperty("card_holder_name")]
        //public string Name { get; set; }

        //[JsonProperty("expiry_date")]
        //public string ExpiryDate { get; set; }

        //[JsonProperty("card_bin")]
        //public string cardbin { get; set; }
    }
}