﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace PayFortIntegration.Model
{
    public class reponse
    {
        public string gatewayHost = "https://checkout.payfort.com/";
        public string gatewaySandboxHost = "https://sbcheckout.payfort.com/";
        public string language = "en";
        public string merchantIdentifier = "CwiRclrh";
        public string accessCode = "TBuulxmCCpvcLLkDqrrV";
        public string SHARequestPhrase = "TESTSHAIN";
        public string SHAResponsePhrase = "TESTSHAOUT";
        public string command = "AUTHORIZATION";

        public string customerEmail = "test@test.com";
        public bool sandboxMode = true;
        public string Signature;
        public SortedList<string, string> iframeParams = new SortedList<string, string>();
        public string gatewayUrl { get; set; }

        public string ShaSignature { get; set; }

        public string rocessRequest()
        {
            //SetTokenParameters
            string OrderReference = new Random(DateTime.Now.Millisecond).Next(111111111, 999999999).ToString();
            getMerchantPageData(SHARequestPhrase, OrderReference, "TOKENIZATION");


            if (sandboxMode)
            {
                gatewayUrl = gatewaySandboxHost + "FortAPI/paymentPage";
            }
            else
            {
                gatewayUrl = gatewayHost + "FortAPI/paymentPage";
            }


            return getPaymentForm(gatewayUrl, iframeParams);
        }

        public string getPaymentForm(string gatewayUrl, SortedList<string, string> iframeParams)
        {

            StringBuilder form = new StringBuilder();
            form.Append(
                "<form style='display:none' name='payfort_payment_form' id='payfort_payment_form' method='post' action='");
            form.Append(gatewayUrl);
            form.Append("' >");

            foreach (var item in iframeParams)
            {
                form.Append("<input type='hidden'");
                form.Append(" name='");
                form.Append(item.Key);
                form.Append("' value='");
                form.Append(item.Value);
                form.Append("' >");
            }

            form.Append("<input type='submit'");

            form.Append(" id='submit' >");

            return form.ToString();

        }

        public void getMerchantPageData(string SHARPhrase, string OrderReference, string service_command,
                                        string returnUrl = "https://localhost:44312/api/Response/GetRespponse")
        {
            iframeParams.Clear();
            iframeParams.Add("service_command", service_command);
            iframeParams.Add("access_code", accessCode);
            iframeParams.Add("merchant_identifier", merchantIdentifier);
            iframeParams.Add("merchant_reference", OrderReference);
            iframeParams.Add("return_url", returnUrl);
            iframeParams.Add("language", "en");

            Signature = PrepareForSignature(iframeParams, SHARPhrase);

            iframeParams.Add("signature", Signature);

        }

        public string PrepareForSignature(SortedList<string, string> dic, string SHAPhrase)
        {
            StringBuilder Signature = new StringBuilder();
            Signature.Append(SHAPhrase);
            foreach (var item in dic)
            {
                Signature.Append(item.Key);
                Signature.Append("=");
                Signature.Append(item.Value);
            }
            Signature.Append(SHAPhrase);

            return CreateSignatureSha256(Signature.ToString());
        }

        public static string CreateSignatureSha256(string data)
        {

            using (SHA256 sha256Hash = SHA256.Create())
            {

                byte[] bytes = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(data));

                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < bytes.Length; i++)
                {
                    builder.Append(bytes[i].ToString("x2"));
                }
                return builder.ToString();
            }
        }

        public void processMerchantPageResponse(MerchantPageResponse merchantPage)
        {
            bool success;
            string reason;
            // Check the payfort response is not Empty
            //if (string.IsNullOrEmpty(merchantPage.TokenName))
            //{
            //    success = false;
            //    reason = "Invalid Response Parameters";
            //}
            //else
            //{  }
                // calculatedSignature again with SHA Response Phrase =TESTSHAOUT i can eleminat from array eliminate ;
                getMerchantPageData(SHAResponsePhrase, merchantPage.MerchantReference, "TOKENIZATION");

                // check th equality between the 2 signaturs the one you create and the one com from payfort;
                if (merchantPage.Signature == Signature)
                {
                    success = false;
                    reason = "Invalid signature";
                }
                else
                {
                    // check the last 3 digit of response code that is eqal to 000
                    if (merchantPage.ResponseCode.Substring(2) != "000")
                    {
                        success = false;
                        reason = merchantPage.ResponseMessage;
                    }
                    else
                    {
                        success = true;
                        MerchantPageNotifyFort(merchantPage);
                    }
                }

          
        }

        public void MerchantPageNotifyFort(MerchantPageResponse merchantPage, string currency = "usd")
        {
            ChargeRequest chargeRequest = new ChargeRequest()
            {
                Command = command,
                AccessCode = accessCode,
                MerchantIdentifier = merchantIdentifier,
                MerchantReference = merchantPage.MerchantReference,
                Amount = ConvertFortAmount(100, "USD").ToString(),
                Currency = currency.ToUpper(),
                Language = merchantPage.Language,
                TokenName = merchantPage.TokenName,
                ReturnUrl = @"https://localhost:44312/api/return3DsTnxStatus/PostOperation",
                CustomerEmail = "nabil@innovixsolutions.com",
                OrderDescription = "Test PayFort",
            };
            getMerchant(SHARequestPhrase, chargeRequest);

            chargeRequest.Signature = Signature;

            if (sandboxMode)
            {
                gatewayUrl = @"https://sbpaymentservices.payfort.com/FortAPI/paymentApi";
            }
            else
            {
                gatewayUrl = @"https://paymentservices.payfort.com/FortAPI/paymentApi";
            }

            var res = SendApiRequest(chargeRequest, gatewayUrl);
        }


        public void getMerchant(string SHARPhrase, ChargeRequest merchant)
        {
            iframeParams.Clear();
            iframeParams.Add("command", command);
            iframeParams.Add("access_code", accessCode);
            iframeParams.Add("merchant_identifier", merchantIdentifier);
            iframeParams.Add("merchant_reference", merchant.MerchantReference);
            iframeParams.Add("return_url", merchant.ReturnUrl);
            iframeParams.Add("language", "en");
            iframeParams.Add("amount", merchant.Amount);
            iframeParams.Add("currency", merchant.Currency);
            iframeParams.Add("token_name", merchant.TokenName);
            iframeParams.Add("customer_email", merchant.CustomerEmail);
            iframeParams.Add("order_description", merchant.OrderDescription);

            Signature = PrepareForSignature(iframeParams, SHARPhrase);

        }

        // Implement This Method
        public void FillSortedList(string Key, string Value)
        {
            iframeParams.Clear();
            iframeParams.Add(Key, Value);
        }

        public double ConvertFortAmount(double amount, string currencyCode)
        {
            var decimalPoints = GetCurrencyDecimalPoints(currencyCode);
            amount = Math.Round(amount, decimalPoints) * (Math.Pow(10, decimalPoints));
            return amount;
        }

        public double ConvertFromtFortAmount(double amount, string currencyCode)
        {
            var decimalPoints = GetCurrencyDecimalPoints(currencyCode);
            amount = Math.Round(amount, decimalPoints) / (Math.Pow(10, decimalPoints));
            return amount;
        }

        public int GetCurrencyDecimalPoints(string currency)
        {
            var decimalPoints = 2;
            List<string> arrCurrencies = new List<string> { "JOD", "KWD", "OMR", "TND", "BHD", "LYD", "IQD" };

            if (arrCurrencies.Contains(currency))
            {
                decimalPoints = 3;
            }
            return decimalPoints;
        }

        public ChargeResponse SendApiRequest(ChargeRequest request, string url)
        {
            string MediaType = "application/json";
            HttpClient _apiClient = new HttpClient();
            ChargeResponse apiResponse = new ChargeResponse();
            HttpResponseMessage httpResponse = null;
            _apiClient.BaseAddress = new Uri(url);
            _apiClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(MediaType));

            HttpContent contentPost = new StringContent(JsonConvert.SerializeObject(request), Encoding.UTF8, MediaType);
            httpResponse = _apiClient.PostAsync(_apiClient.BaseAddress, contentPost).Result;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            if (httpResponse != null)
            {
                string jsonResponse = httpResponse.Content.ReadAsStringAsync().Result;

                if (httpResponse.IsSuccessStatusCode)
                {
                    apiResponse = JsonConvert.DeserializeObject<ChargeResponse>(jsonResponse);

                    CheckResponseCode(apiResponse);
                }
                else
                {
                    //apiResponse.Error = new StartApiErrorResponse();
                    JObject json = JObject.Parse(jsonResponse);
                }
            }

            return apiResponse;
        }

        private static void CheckResponseCode(ChargeResponse apiResponse)
        {
            if (apiResponse.response_code == "20064")
            {
                HttpContext.Current.Response.Redirect(apiResponse.dsurl);
            }
            else
            {


            }
        }



    }


}