﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PayFortIntegration.Model
{
    [JsonObject(MemberSerialization.OptIn)]
    public class MerchantPageResponse
    {
       
        [JsonProperty("service_command")]
        public string ServiceCommand { get; set; }

        [JsonProperty("access_code")]
        public string AccessCode { get; set; }

        [JsonProperty("merchant_identifier")]
        public string MerchantIdentifier { get; set; }

        [JsonProperty("merchant_reference")]
        public string MerchantReference { get; set; }

        [JsonProperty("signature")]
        public string Signature { get; set; }

        [JsonProperty("language")]
        public string Language { get; set; }

        [JsonProperty("response_message")]
        public string ResponseMessage { get; set; }

        [JsonProperty("response_code")]
        public string ResponseCode { get; set; }

        [JsonProperty("token_name")]
        public string TokenName { get; set; }

        [JsonProperty("return_url")]
        public string ReturnUrl { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("card_number")]
        public string CardNumber { get; set; }

        [JsonProperty("card_holder_name")]
        public string Name { get; set; }

        [JsonProperty("expiry_date")]
        public string ExpiryDate { get; set; }

        [JsonProperty("card_bin")]
        public string Cardbin { get; set; }
    }
}