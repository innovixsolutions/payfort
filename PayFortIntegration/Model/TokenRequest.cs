﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace PayFortIntegration.Model
{
    public class TokenRequest
    {
       
        public TokenRequest()
        {
          
            //string HttpMethod = System.Net.WebRequestMethods.Http.Post;
        }

        [JsonProperty("service_command")]
        public string Service_Command { get; set; }

        [JsonProperty("access_code")]
        public string Access_Code { get; set; }

        [JsonProperty("merchant_identifier")]
        public string Merchant_Identifier { get; set; }

        [JsonProperty("merchant_reference")]
        public string Merchant_Reference { get; set; }

        [JsonProperty("language")]
        public string Language { get; set; } 

        [JsonProperty("signature")]
        public string Signature { get; set; }

        [JsonProperty("expiry_date")]
        public string Expiry_Date { get; set; }

        [JsonProperty("card_number")]
        public string Card_Number { get; set; }

        [JsonProperty("card_security_code")]
        public string Card_Security_Code { get; set; }

        [JsonProperty("return_url")]
        public string Return_Url { get; set; }


    }
}