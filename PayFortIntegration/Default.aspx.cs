﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PayFortIntegration.Model;
using PaymentProcessor.PayFort.DTOs;
using PaymentProcessor.PayFort.Enums;
using PaymentProcessor.PayFort.Managers;
using PaymentProcessor.PayFort.Model;
using PaymentProcessor.PayFort.Infrastructure;


namespace PayFortIntegration
{
    public partial class _Default : Page
    {
       
        public TokenInfo tokenParameters;

        protected void Page_Init(object sender, EventArgs e)
        {
         
            bool testMode = Convert.ToBoolean(ConfigurationManager.AppSettings["UseTestmode"]);
            PayFortManager PayFortProcessor = new PayFortManager(testMode);
            string orderReference = GenerateRandNumber();
            OrderInfo order = new OrderInfo()
            {
                MerchantReference = orderReference,
                Amount = 500,
                Currency = PayFortInterfaceISOCurrencyCode.USD,
                CustomerEmail = "nabil@innovixsolutions.com",
                OrderDescription = "Test Order Description",
                Language = PayFortInterfaceISOLanguageCode.en,
            };
            
            tokenParameters = PayFortProcessor.GetTokenParameters(order);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
          
        }


       

        public string GenerateRandNumber()
        {
            Random rndNum = new Random(int.Parse(Guid.NewGuid().ToString().Substring(0, 8), System.Globalization.NumberStyles.HexNumber));

            int rnd = rndNum.Next(111111111, 999999999);

            return rnd.ToString();
        }

    }
}