﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="PayFortIntegration._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="jumbotron">
        <h1>PayFor Processor</h1>
       
    </div>

    <div class="row">
        <div class="col-md-4">
            
        </div>
        <div class="col-md-4">
          
        </div>
        <div class="col-md-4">
            
        </div>
    </div>

   <div class="h-seperator"></div>

    <section class="payment-method">

                <div class="Wrapper" >
                    <div id="frm_payfort_payment_merchant_page2" class="form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="payfort_fort_mp2_card_holder_name">Name on Card</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="card_holder_name" id="payfort_fort_mp2_card_holder_name" placeholder="Card Holder's Name" maxlength="50">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="payfort_fort_mp2_card_number">Card Number</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="card)number" id="payfort_fort_mp2_card_number" placeholder="Debit/Credit Card Number" maxlength="16">
                            </div>
                        </div>

                        <div class="form-group ">
                            <label class="col-sm-3 control-label" for="payfort_fort_mp2_expiry_month">Expiration Date</label>
                            <div class="col-sm-9">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <select class="form-control col-sm-2" name="expiry_month" id="payfort_fort_mp2_expiry_month">
                                            <option value="01">Jan (01)</option>
                                            <option value="02">Feb (02)</option>
                                            <option value="03">Mar (03)</option>
                                            <option value="04">Apr (04)</option>
                                            <option value="05">May (05)</option>
                                            <option value="06">June (06)</option>
                                            <option value="07">July (07)</option>
                                            <option value="08">Aug (08)</option>
                                            <option value="09">Sep (09)</option>
                                            <option value="10">Oct (10)</option>
                                            <option value="11">Nov (11)</option>
                                            <option value="12">Dec (12)</option>
                                        </select>
                                    </div>
                                    <div class="col-xs-3">
                                        <select class="form-control" name="expiry_year" id="payfort_fort_mp2_expiry_year">
                                          <option value=""></option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="payfort_fort_mp2_cvv">Card CVV</label>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" name="cvv" id="payfort_fort_mp2_cvv" placeholder="Security Code" maxlength="4">
                            </div>
                        </div>
                    </div>
                </div>
         
    </section>

    <div class="h-seperator"></div>

    <section class="actions">
   
    </section>
    

</asp:Content>
