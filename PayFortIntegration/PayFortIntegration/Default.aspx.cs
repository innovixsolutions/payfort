﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PayFortIntegration.Model;
using PayFortIntegration.Service;


namespace PayFortIntegration
{
    public partial class _Default : Page
    {
        TokenRequest tokenRequest;
        BaseService baseService = new BaseService();
        protected void Page_Load(object sender, EventArgs e)
        {
            tokenRequest = new TokenRequest() {
                Access_Code= "TBuulxmCCpvcLLkDqrrV",
                Merchant_Identifier= "CwiRclrh",
                Merchant_Reference= "XYZ9239-yu898",
                Signature= CreateDictionaryFromClass(),
                Card_Number= "5123456789012346",
                Expiry_Date= "0521",
                Card_Security_Code= "123",

            };

            CreateToken(tokenRequest);

        }

        public ApiResponse<Token> CreateToken(TokenRequest request)
        {
            ApiResponse<Token> response = baseService.SendApiRequest<TokenRequest, Token>(request);
            return response;
        }

        public string CreateDictionaryFromClass()
        {
            SortedList<string, string> h = new SortedList<string, string>();
            h.Add("service_command", "TOKENIZATION");
            h.Add("access_code", "TBuulxmCCpvcLLkDqrrV");
            h.Add("merchant_identifier", "CwiRclrh");
            h.Add("merchant_reference", "XYZ9239-yu898");
            h.Add("language", "en");

            return PrepareForSignature(h);
        }

        public string PrepareForSignature(SortedList<string, string> dic)
        {
            StringBuilder Signature = new StringBuilder();
            Signature.Append("TESTSHAIN");
            foreach (var item in dic)
            {
                Signature.Append(item.Key);
                Signature.Append("=");
                Signature.Append(item.Value);
            }
            Signature.Append("TESTSHAIN");

            return CreateSignatureSha256(Signature.ToString());
        }

        

        static string CreateSignatureSha256(string data)
        {

            using (SHA256 sha256Hash = SHA256.Create())
            {

                byte[] bytes = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(data));

                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < bytes.Length; i++)
                {
                    builder.Append(bytes[i].ToString("x2"));
                }
                return builder.ToString();
            }
        }
    }
}