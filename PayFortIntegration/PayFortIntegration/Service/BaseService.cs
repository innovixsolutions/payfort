﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using PayFortIntegration.Model;
using System.Web.Script.Serialization;


namespace PayFortIntegration.Service
{
    public class BaseService
    {
        //string Url = ConfigurationManager.AppSettings["URL"].ToString();
        private string EndpointBase = @"https://sbcheckout.PayFort.com/FortAPI/paymentPage";
        public string MediaType = "application/json";
        private HttpClient _apiClient { get; set; }
        //string HttpMethod = System.Net.WebRequestMethods.Http.Post;

        public BaseService()
        {
            _apiClient = new HttpClient();
            _apiClient.BaseAddress = new Uri(EndpointBase);

            //_apiClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(MediaType));
            //_apiClient.DefaultRequestHeaders.Add("Authorization", GetAuthorizationHeaderValue(privateKey));

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
        }




        public ApiResponse<ResponseType> SendApiRequest<TokenRequest, ResponseType>(TokenRequest request)
             where ResponseType : class, new()
        {
            //using (WebClient wc = new WebClient())
            //{

            //    wc.Headers[HttpRequestHeader.ContentType] = "application/json";
            //wc.Headers[HttpRequestHeader.Authorization]
            //wc.Headers[HttpRequestHeader.TenantId]
            //wc.Headers[HttpRequestHeader.Client - Type]
            //wc.Headers[HttpRequestHeader.Protocol]
            //    //wc.Headers.Add("Content-Type", "application/json");
            //    string HtmlResult = wc.UploadString(EndpointBase, JsonConvert.SerializeObject(request));
            //}
            JsonConvert.SerializeObject(request);

            ApiResponse<ResponseType> apiResponse = new ApiResponse<ResponseType>();
            HttpResponseMessage httpResponse = null;

            HttpContent contentPost = new StringContent(JsonConvert.SerializeObject(request), Encoding.UTF8, MediaType);
            httpResponse = _apiClient.PostAsync("", contentPost).Result;

            if (httpResponse != null)
            {
               
                string jsonResponse = httpResponse.Content.ReadAsStringAsync().Result;

                

                if (httpResponse.IsSuccessStatusCode)
                {
                    
                    apiResponse.Content = JsonConvert.DeserializeObject<ResponseType>(jsonResponse);
                }

                else
                {
                    apiResponse.Error = new StartApiErrorResponse();
                    JObject json = JObject.Parse(jsonResponse);
                    
                }
            }

            return apiResponse;
        }
      
    }
}