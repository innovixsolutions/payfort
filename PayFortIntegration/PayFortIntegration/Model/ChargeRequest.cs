﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PayFortIntegration.Model
{
    public class ChargeRequest
    {

        [JsonProperty("command")]
        public string Command { get; set; } = "AUTHORIZATION";

        [JsonProperty("access_code")]
        public string Access_Code { get; set; }

        [JsonProperty("merchant_identifier")]
        public string Merchant_Identifier { get; set; }

        [JsonProperty("merchant_reference")]
        public string Merchant_Reference { get; set; }

        [JsonProperty("amount")]
        public string Amount { get; set; }

        [JsonProperty("currency")]
        public string Currency { get; set; }

        [JsonProperty("language")]
        public string Language { get; set; } = "en";

        [JsonProperty("customer_email")]
        public string Customer_Email { get; set; } 

        [JsonProperty("token_name")]
        public string token_name { get; set; }

        [JsonProperty("signature")]
        public string Signature { get; set; }

    }
}