﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaymentProcessor.PayFort.Enums
{
    public enum PayFortInterfaceISOCurrencyCode
    {
        USD,
        EGP,
        AED
    }
}
