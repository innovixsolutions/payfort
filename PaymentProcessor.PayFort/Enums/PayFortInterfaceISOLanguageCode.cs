﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaymentProcessor.PayFort.Enums
{
    public enum PayFortInterfaceISOLanguageCode
    {
        en,
        ar
    }

}
