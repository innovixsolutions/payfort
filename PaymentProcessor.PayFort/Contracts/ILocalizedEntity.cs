using System;
using System.Collections.Generic;
using System.Linq;

namespace PaymentProcessor.PayFort.Contracts
{
    public interface ILocalizedEntity
    {
        int DefaultLanguageId { get; set; }
        void Localize(int languageId);
    }
}
