﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PaymentProcessor.PayFort.Model;

namespace PaymentProcessor.PayFort.Repositories
{
   public class PayFortMerchanetRepository : BaseRepository<PayFortMerchant, PayFortIntegrationEntities>
    {
        public PayFortMerchanetRepository(int langugeId) 
            :base(langugeId) {}

        public PayFortMerchant GetMerchant(string merchantId)
        {
           return GetOne(x => x.MerchantIdentifier == merchantId);
        }
    }
}
