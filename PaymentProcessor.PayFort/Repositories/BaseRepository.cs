using PaymentProcessor.PayFort.Contracts;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Web;


namespace PaymentProcessor.PayFort.Repositories
{
    public abstract class BaseRepository<TEntity, TContext> : IDisposable
        where TEntity : class
        where TContext : DbContext, new()
    {

        protected int? LanguageId;
        protected TContext Context
        {
            get
            {
                var dbKey = typeof(TContext).FullName + HttpContext.Current.GetHashCode();
                if (!HttpContext.Current.Items.Contains(dbKey))
                    HttpContext.Current.Items.Add(dbKey, new TContext());
                return HttpContext.Current.Items[dbKey] as TContext;
            }
        }

        public BaseRepository(int? languageId = null)
        {
            LanguageId = languageId;
        }

        #region CRUD
        public virtual TEntity Create(TEntity entity)
        {
            entity = Context.Set<TEntity>().Add(entity);
            Save();
            return entity;
        }

        public virtual TEntity Update(TEntity entity)
        {
            Context.Entry(entity).State = EntityState.Modified;
            Save();
            return entity;
        }

        public virtual void Delete(int id)
        {
            var item = Context.Set<TEntity>().Find(id);
            Context.Set<TEntity>().Remove(item);
            Save();
        }

        public virtual void Delete(TEntity entity)
        {
            Context.Set<TEntity>().Remove(entity);
            Save();
        }

        public virtual void Delete(Expression<Func<TEntity, bool>> where)
        {
            var objects = Context.Set<TEntity>().Where(where);
            foreach (var item in objects)
            {
                Context.Set<TEntity>().Remove(item);
            }
            Save();
        }
        #endregion

        #region Fetch operations
        public virtual TEntity GetOne(Expression<Func<TEntity, bool>> where, params Expression<Func<TEntity, object>>[] includeProperties)
        {
            return GetMany(where, includeProperties).FirstOrDefault();

        }

        public virtual IQueryable<TEntity> GetMany(Expression<Func<TEntity, bool>> where = null,
            params Expression<Func<TEntity, object>>[] includeProperties)
        {
            var query = where == null
                ? Context.Set<TEntity>()
                : Context.Set<TEntity>().Where(where);
            var entities = includeProperties.Aggregate(query, (current, includeProperty) =>
                current.Include(includeProperty));

            entities = Localize(entities, includeProperties);
            return entities;
        }



        public virtual IQueryable<TEntity> GetAll()
        {
            IQueryable<TEntity> entities = Context.Set<TEntity>();
            entities = Localize(entities);
            return entities;
        }

        public virtual IQueryable<TEntity> GetMany(Expression<Func<TEntity, bool>> where = null)
        {
            var entities = null != where ? Context.Set<TEntity>().Where(where) : Context.Set<TEntity>();
            entities = Localize(entities);
            return entities;
        }

        #endregion

        #region Context Operations
        public void Dispose()
        {
            if (null != Context)
            {
                Context.Dispose();
            }
        }

        private void Save()
        {
            string error;
            try
            {
                Context.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                error = GetValidationErrors(ex);
                throw new Exception(error);
            }
            catch (Exception ex)
            {
                error = string.IsNullOrWhiteSpace(ex.InnerException.Message) ? ex.Message : ex.InnerException.Message;
                throw new Exception(error);
            }
        }

        private static string GetValidationErrors(DbEntityValidationException ex)
        {
            var errors = ex.EntityValidationErrors;
            var builder = new StringBuilder("Entity Validation Errors: <br />");
            foreach (var entity in errors)
            {
                builder.AppendFormat("{0} <br />", entity.Entry.Entity);
                foreach (var error in entity.ValidationErrors)
                {
                    builder.AppendFormat("{0} <br />", error.ErrorMessage);
                }
            }
            return builder.ToString();
        }

        #endregion

        #region Localization Methods
        private IQueryable<TEntity> Localize(IQueryable<TEntity> entities, Expression<Func<TEntity, object>>[] includeProperties = null)
        {

            //Make sure to include the localized entity
            entities = IncludeLocalizedEntities<TEntity>(entities, typeof(TEntity));
            //Include all the localized entities for the includes
            if (includeProperties != null)
                entities = includeProperties.Aggregate(entities,
                    (current, expression) => IncludeLocalizedEntities(current, GetObjectType(expression), expression));

            foreach (TEntity entity in entities.AsEnumerable())
            {
                Localize(entity, includeProperties);
            }
            return entities;
        }

        private void Localize(TEntity entity, params Expression<Func<TEntity, object>>[] includeProperties)
        {
            if (IsLocalized(typeof(TEntity)))
                ((ILocalizedEntity)entity).Localize(LanguageId.Value);

            if (includeProperties != null)
                foreach (var expression in includeProperties)
                {
                    var includeType = GetObjectType<TEntity>(expression);
                    var nonGenericType = GetObjectGenericType<TEntity>(expression);

                    var navigationProperty = GetPropertyNavigationString(expression);

                    var localizedEntity = GetProp(typeof(TEntity), navigationProperty);

                    if (IsLocalized(nonGenericType) && localizedEntity != null)
                    {
                        if (includeType.IsGenericType)
                        {
                            var childEntities = GetPropValue<TEntity>(entity, navigationProperty) as IEnumerable;
                            if (childEntities != null)
                                foreach (var childEntity in childEntities)
                                {
                                    if (childEntity != null)
                                        ((ILocalizedEntity)childEntity).Localize(LanguageId.Value);
                                }
                        }
                        else
                        {
                            var property = (ILocalizedEntity)GetPropValue<TEntity>(entity, navigationProperty);
                            if (property != null)
                                property.Localize(LanguageId.Value);
                        }

                    }
                }
        }
        private IQueryable<TEntity> IncludeLocalizedEntities<T>(IQueryable<TEntity> query, Type type, Expression<Func<T, object>> expr = null)
        {
            if (type.IsGenericType)
            {
                type = type.GetGenericArguments()[0];
            }
            //Check for localization
            if (IsLocalized(type))
            {
                IEnumerable<PropertyInfo> localizedTypes = type.GetProperties()
                       .Where(x => x.Name.Contains("Localized"));

                if (localizedTypes.Any())
                {
                    if (type == typeof(TEntity))
                        return localizedTypes.Aggregate(query, (current, localizedType) =>
                            current.Include(localizedType.Name));
                    else
                    {
                        var nvaigationString = GetPropertyNavigationString(expr);

                        return localizedTypes.Aggregate(query, (current, localizedType) =>
                            current.Include(nvaigationString + "." + localizedType.Name));
                    }
                }
            }
            return query;
        }
        #endregion

        #region Reflection Operations
        private string GetPropertyNavigationString(LambdaExpression expr, string breadCrumb = null)
        {
            string nvaigationString = null;
            if ((expr.Body as MemberExpression) != null)
            {
                var memberNames = new Stack<string>();

                MemberExpression memberExp = (MemberExpression)expr.Body;
                memberNames.Push(memberExp.Member.Name);
                while (memberExp.Expression is MemberExpression && TryFindMemberExpression(memberExp.Expression, out memberExp))
                {
                    memberNames.Push(memberExp.Member.Name);
                }

                nvaigationString = string.Join(".", memberNames.ToArray());
            }

            else if ((expr.Body as MethodCallExpression) != null)
            {
                var experssionArguments = ((MethodCallExpression)expr.Body).Arguments;
                breadCrumb += ((MemberExpression)experssionArguments[0]).Member.Name + ".";

                breadCrumb = experssionArguments.Skip(1)
                    .Aggregate(breadCrumb,
                        (current, argument) =>
                            current + (GetPropertyNavigationString(argument as LambdaExpression, current) + "."));
                nvaigationString = breadCrumb.TrimEnd('.');
            }
            return nvaigationString.TrimEnd('.');
        }

        private bool TryFindMemberExpression(Expression expr, out MemberExpression memberExp)
        {
            memberExp = expr as MemberExpression;
            if (memberExp != null)
            {
                return true;
            }
            if ((expr.NodeType == ExpressionType.Convert) ||
                (expr.NodeType == ExpressionType.ConvertChecked) && expr is UnaryExpression)
            {
                memberExp = ((UnaryExpression)expr).Operand as MemberExpression;
                if (memberExp != null)
                {
                    return true;
                }
            }

            return false;
        }

        public PropertyInfo GetProp(Type baseType, string propertyName)
        {
            string[] parts = propertyName.Split('.');

            if (baseType.IsGenericType)
                baseType = baseType.GenericTypeArguments[0];

            if (parts.Length > 1)
            {
                return GetProp(baseType.GetProperty(parts[0]).PropertyType,
                    parts.Skip(1).Aggregate((a, i) => a + "." + i));
            }

            return baseType.GetProperty(propertyName);
        }

        public static object GetPropValue<T>(T obj, string propName)
        {
            string[] nameParts = propName.Split('.');
            var type = obj.GetType();

            if (nameParts.Length == 1)
            {
                if (type.IsGenericType)
                {
                    return (from object childEntity in (IEnumerable)obj select type.GenericTypeArguments[0].GetProperty(propName).GetValue(childEntity, null)).ToList();
                }
                return type.GetProperty(propName).GetValue(obj, null);
            }

            foreach (string part in nameParts)
            {
                if (type.IsGenericType)
                {
                    var values = new List<object>();
                    foreach (var childEntity in (IEnumerable)obj)
                    {
                        PropertyInfo info = type.GenericTypeArguments[0].GetProperty(part);
                        if (info == null)
                        { return null; }

                        values.Add(GetPropValue(info.GetValue(childEntity, null),
                            string.Join(".", nameParts.Skip(1))));
                    }
                    return values;
                }
                else
                {
                    PropertyInfo info = type.GetProperty(part);
                    if (info == null)
                    { return null; }

                    return GetPropValue(info.GetValue(obj, null),
                            string.Join(".", nameParts.Skip(1)));
                }

            }
            return obj;
        }

        private Type GetObjectType<T>(Expression<Func<T, object>> expr)
        {
            if ((expr.Body.NodeType == ExpressionType.Convert) ||
                (expr.Body.NodeType == ExpressionType.ConvertChecked))
            {
                var unary = expr.Body as UnaryExpression;
                if (unary != null)
                    return unary.Operand.Type;
            }
            return expr.Body.Type;
        }

        private Type GetObjectGenericType<T>(Expression<Func<T, object>> expr)
        {
            Type type = GetObjectType<T>(expr);
            if (type.IsGenericType)
                type = type.GetGenericArguments().FirstOrDefault();
            return type;
        }

        private bool IsLocalized(Type type)
        {
            return typeof(ILocalizedEntity).IsAssignableFrom(type) && LanguageId.HasValue;
        }
        #endregion

    }
}