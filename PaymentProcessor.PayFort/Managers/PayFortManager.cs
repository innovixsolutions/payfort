﻿using PaymentProcessor.PayFort.DTOs;
using PaymentProcessor.PayFort.Enums;
using PaymentProcessor.PayFort.Infrastructure;
using PaymentProcessor.PayFort.Model;
using PaymentProcessor.PayFort.Repositories;
using System.Collections.Generic;
using System.Configuration;
using System.Text;
using System.Web;

namespace PaymentProcessor.PayFort.Managers
{
    public class PayFortManager
    {
        private string tokenRequestCommand = "TOKENIZATION";
        private string purchaseCommand = "PURCHASE";
        //private string authorizationCommand = "AUTHORIZATION";

        private PayFortMerchant merchant;
        private readonly PayFortMerchanetRepository merchantRepository;
        private readonly bool testMode;
        private readonly string currentHostName;


        public PayFortManager(bool useTestMode)
        {
            testMode = useTestMode;
            currentHostName = PayFortHelpers.GetCurrentHostName();
            var merchantId = string.Empty;
            if (testMode == true)
            {
                merchantId = ConfigurationManager.AppSettings["TestMerchantId"];
            }
            else
            {
                merchantId = ConfigurationManager.AppSettings["LiveMerchantId"];
            }

            merchantRepository = new PayFortMerchanetRepository(2057);
            merchant = merchantRepository.GetMerchant(merchantId);
        }

        public TokenInfo GetTokenParameters(OrderInfo orderInfo)
        {
            HttpContext.Current.Session["OrderInfo"] = orderInfo;

            TokenInfo tokenParameters = new TokenInfo()
            {
                AccessCode = merchant.AccessCode,
                MerchantIdentifier = merchant.MerchantIdentifier,
                MerchantReference = orderInfo.MerchantReference,
                Language = orderInfo.Language.ToString(),
                ServiceCommand = tokenRequestCommand,
                ReturnUrl = $"{currentHostName}/api/TokenResponse/GetTokenResponse",
                SHARPhrase = merchant.SHARequestPhrase
            };

            tokenParameters.Signature = GetTokenRequestSignature(tokenParameters);
            return tokenParameters;
        }

        public void ProcessTokenResponse()
        {
            OrderInfo orderInfo = (OrderInfo)HttpContext.Current.Session["OrderInfo"];
            const string TokenizationSuccessResonseCode = "18000";

            var request = HttpContext.Current.Request;
        

            if ((request.Params.GetValues("response_code")[0].ToString()) != TokenizationSuccessResonseCode && (request.Params.GetValues("response_code")[0].ToString()).Substring(2) != "000")
            {

                HttpContext.Current.Response.Redirect($"{PayFortHelpers.GetCurrentHostName()}/PaymentFailed.aspx?Message={request.Params.GetValues("response_message")[0].ToString()}");
            }
            else
            {
                TokenResponse TokenizationResponse = new TokenResponse()
                {
                    TokenName = request.Params.GetValues("token_name")[0].ToString(),
                    MerchantIdentifier = request.Params.GetValues("merchant_identifier")[0].ToString(),
                    MerchantReference = request.Params.GetValues("merchant_reference")[0].ToString(),
                    Signature = request.Params.GetValues("signature")[0].ToString(),
                    AccessCode = request.Params.GetValues("access_code")[0].ToString(),
                    ResponseCode = request.Params.GetValues("response_code")[0].ToString(),
                    ResponseMessage = request.Params.GetValues("response_message")[0].ToString(),
                    Language = request.Params.GetValues("language")[0].ToString(),
                    ServiceCommand = request.Params.GetValues("service_command")[0].ToString(),
                    Status = request.Params.GetValues("status")[0].ToString(),
                    CardNumber = request.Params.GetValues("card_number")[0].ToString(),
                    ExpiryDate = request.Params.GetValues("expiry_date")[0].ToString(),
                    Cardbin = request.Params.GetValues("card_bin")[0].ToString(),
                    Name = request.Params.GetValues("card_holder_name") == null ? " " : request.Params.GetValues("card_holder_name")[0].ToString(),
                };

                CreatePurchaseRequest(TokenizationResponse, orderInfo);
            }

        }

        public void ProcessPurchaseResponse()
        {
            var headerContext = HttpContext.Current.Request;
            StringBuilder stringBuilder = new StringBuilder();
            string Url = $"{PayFortHelpers.GetCurrentHostName()}/RoutePage.aspx";
            stringBuilder.Append(Url);
            stringBuilder.Append("?");
            foreach (string key in headerContext.Form.AllKeys)
            {
                stringBuilder.Append(key);
                stringBuilder.Append("=");
                stringBuilder.Append(headerContext.Params.GetValues(key)[0].ToString());
                stringBuilder.Append("&");
            }
            HttpContext.Current.Response.Redirect(stringBuilder.ToString().TrimEnd(new char[] { '&' }));
        }

        private void CreatePurchaseRequest(TokenResponse tokenResponse, OrderInfo orderInfo) //TODO: convert currency to enum
        {
            PayFortApiManager callPaymentApi = new PayFortApiManager();
            PurchaseRequest chargeRequest = new PurchaseRequest()
            {
                Command = purchaseCommand,
                AccessCode = tokenResponse.AccessCode,
                MerchantIdentifier = tokenResponse.MerchantIdentifier,
                MerchantReference = tokenResponse.MerchantReference,
                Amount = PayFortHelpers.GetAmountInIso3DecimalFormat(orderInfo.Amount.ToString(), PayFortInterfaceISOCurrencyCode.USD),
                Currency = orderInfo.Currency.ToString(),
                Language = tokenResponse.Language,
                TokenName = tokenResponse.TokenName,
                ReturnUrl = $"{currentHostName}/api/PurchaseResponse/GetPurchaseResponse",
                CustomerEmail = orderInfo.CustomerEmail,
                OrderDescription = orderInfo.OrderDescription,
            };

            chargeRequest.Signature = GetPurchaseRequestSignature(merchant.SHARequestPhrase, chargeRequest);

            var res = callPaymentApi.SendApiRequest(chargeRequest, merchant.GatewayPaymentUrl);
        }

        private string GetPurchaseRequestSignature(string SHARPhrase, PurchaseRequest merchant)
        {
            SortedList<string, string> sortedChargeParameters = new SortedList<string, string>();
            sortedChargeParameters.Clear();
            sortedChargeParameters.Add("command", purchaseCommand);
            sortedChargeParameters.Add("access_code", merchant.AccessCode);
            sortedChargeParameters.Add("merchant_identifier", merchant.MerchantIdentifier);
            sortedChargeParameters.Add("merchant_reference", merchant.MerchantReference);
            sortedChargeParameters.Add("return_url", merchant.ReturnUrl);
            sortedChargeParameters.Add("language", merchant.Language);
            sortedChargeParameters.Add("amount", merchant.Amount);
            sortedChargeParameters.Add("currency", merchant.Currency);
            sortedChargeParameters.Add("token_name", merchant.TokenName);
            sortedChargeParameters.Add("customer_email", merchant.CustomerEmail);
            sortedChargeParameters.Add("order_description", merchant.OrderDescription);

            return PayFortHelpers.GenerateSignature(sortedChargeParameters, SHARPhrase);

        }

        private string GetTokenRequestSignature(TokenInfo tokenParameters)
        {
            SortedList<string, string> sortedTokenParameters = new SortedList<string, string>();
            sortedTokenParameters.Clear();
            sortedTokenParameters.Add("service_command", tokenParameters.ServiceCommand);
            sortedTokenParameters.Add("access_code", tokenParameters.AccessCode);
            sortedTokenParameters.Add("merchant_identifier", tokenParameters.MerchantIdentifier);
            sortedTokenParameters.Add("merchant_reference", tokenParameters.MerchantReference);
            sortedTokenParameters.Add("return_url", tokenParameters.ReturnUrl);
            sortedTokenParameters.Add("language", tokenParameters.Language);

            return PayFortHelpers.GenerateSignature(sortedTokenParameters, tokenParameters.SHARPhrase);
        }
    }
}
