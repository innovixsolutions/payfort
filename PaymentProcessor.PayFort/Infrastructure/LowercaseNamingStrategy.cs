﻿using Newtonsoft.Json.Serialization;

namespace PaymentProcessor.PayFort.Infrastructure
{
    public class LowercaseNamingStrategy : NamingStrategy
    {
        protected override string ResolvePropertyName(string name)
        {
            return name.ToLowerInvariant();
        }
    }


}