﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using PaymentProcessor.PayFort.Enums;
using System.ComponentModel;

namespace PaymentProcessor.PayFort.Infrastructure
{
    public static class PayFortHelpers
    {
        public static string GenerateRandomNumber()
        {
            Random rndNum = new Random(int.Parse(Guid.NewGuid().ToString().Substring(0, 8), System.Globalization.NumberStyles.HexNumber));

            int rnd = rndNum.Next(111111111, 999999999);

            return rnd.ToString();
        }

        public static string GetCurrentHostName()
        {
            var request = HttpContext.Current.Request;
            var port = (request.Url.IsDefaultPort ? "" : ":" + request.Url.Port);
            string currentHostUrl = $"{request.Url.Scheme}{Uri.SchemeDelimiter}{request.Url.Host}{port}";

            return currentHostUrl;
        }

        public static string GetSHA256Hash(string data)
        {

            using (SHA256 sha256Hash = SHA256.Create())
            {
                byte[] bytes = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(data));

                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < bytes.Length; i++)
                {
                    builder.Append(bytes[i].ToString("x2"));
                }
                return builder.ToString();
            }
        }

        public static string GetAmountInIso3DecimalFormat(string amount, PayFortInterfaceISOCurrencyCode currencyCode) 
        {
            var decimalPoints = GetCurrencyDecimalPoints(currencyCode.ToString());
           double NewAmount = Convert.ToDouble(amount);
            NewAmount = Math.Round(NewAmount, decimalPoints) * (Math.Pow(10, decimalPoints));
            return NewAmount.ToString();
        }

        public static string GetAmountInStandardFormat(string amount, PayFortInterfaceISOCurrencyCode currencyCode)
        {
            var decimalPoints = GetCurrencyDecimalPoints(currencyCode.ToString());
            double NewAmount = Convert.ToDouble(amount);
            NewAmount = Math.Round(NewAmount, decimalPoints) / (Math.Pow(10, decimalPoints));
            return NewAmount.ToString();
        }

        private static int GetCurrencyDecimalPoints(string currency)
        {
            var decimalPoints = 2;
            List<string> arrCurrencies = new List<string> { "JOD", "KWD", "OMR", "TND", "BHD", "LYD", "IQD" };

            if (arrCurrencies.Contains(currency))
            {
                decimalPoints = 3;
            }
            return decimalPoints;
        }

        internal static string GenerateSignature(SortedList<string, string> parameters, string SHAPhrase)
        {
            StringBuilder Signature = new StringBuilder();
            Signature.Append(SHAPhrase);
            foreach (var item in parameters)
            {
                Signature.Append(item.Key);
                Signature.Append("=");
                Signature.Append(item.Value);
            }
            Signature.Append(SHAPhrase);

            return PayFortHelpers.GetSHA256Hash(Signature.ToString());
        }
    }
}
