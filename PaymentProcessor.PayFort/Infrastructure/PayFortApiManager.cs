﻿using System;
using System.Web;
using System.Net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using PaymentProcessor.PayFort.Model;

namespace PaymentProcessor.PayFort.Infrastructure
{
    internal class PayFortApiManager
    {
        private string MediaType = "application/json";
        private HttpClient _apiClient { get; set; }
        public PayFortApiManager()
        {
            _apiClient = new HttpClient();
            _apiClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(MediaType));
        }

        public PurchaseResponse SendApiRequest(PurchaseRequest request, string url)
        {
            PurchaseResponse _apiResponse = new PurchaseResponse();
            HttpResponseMessage _httpResponse = new HttpResponseMessage();
            _apiClient.BaseAddress = new Uri(url);

            HttpContent contentPost = new StringContent(JsonConvert.SerializeObject(request), Encoding.UTF8, MediaType);
            _httpResponse = _apiClient.PostAsync(_apiClient.BaseAddress, contentPost).Result;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            if (_httpResponse != null)
            {
                string jsonResponse = _httpResponse.Content.ReadAsStringAsync().Result;

                if (_httpResponse.IsSuccessStatusCode)
                {
                    _apiResponse = JsonConvert.DeserializeObject<PurchaseResponse>(jsonResponse);
                    CheckResponseCode(_apiResponse);
                }
                else
                {
                    //apiResponse.Error = new StartApiErrorResponse();
                    JObject json = JObject.Parse(jsonResponse);
                }
            }

            return _apiResponse;
        }

        private static void CheckResponseCode(PurchaseResponse apiResponse)
        {
            const string _3DSUrlRedirectionCode = "20064";
            if (apiResponse.ResponseCode == _3DSUrlRedirectionCode)
            {
                HttpContext.Current.Response.Redirect(apiResponse._3DSUrl);
            }
        }
    }
}
