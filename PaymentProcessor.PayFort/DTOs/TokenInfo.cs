﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaymentProcessor.PayFort.Model
{
    public class TokenInfo
    {
        public string ServiceCommand { get; set; }

        public string AccessCode { get; set; }

        public string MerchantIdentifier { get; set; }

        public string MerchantReference { get; set; }

        public string Language { get; set; }

        public string Signature { get; set; }

        public string SHARPhrase { get; set; }

        public string ReturnUrl { get; set; }

        public string Amount { get; set; }

        public string Currency { get; set; }

        public string CustomerEmail { get; set; }

        public string OrderDescription { get; set; }

    }
}
