﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PaymentProcessor.PayFort.Enums;

namespace PaymentProcessor.PayFort.DTOs
{
    public class OrderInfo
    {
        public string MerchantReference { get; set; }
        public PayFortInterfaceISOCurrencyCode Currency { get; set; }
        public decimal Amount { get; set; }
        public string CustomerEmail { get; set; }
        public string OrderDescription { get; set; }
        public PayFortInterfaceISOLanguageCode Language { get; set; }
    }
}
