﻿using Newtonsoft.Json;
using PaymentProcessor.PayFort.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PaymentProcessor.PayFort.Model
{
    [JsonObject(NamingStrategyType = typeof(LowercaseNamingStrategy))]
    public class PurchaseRequest
    {

        public string Command { get; set; }

        [JsonProperty("access_code")]
        public string AccessCode { get; set; }

        [JsonProperty("merchant_identifier")]
        public string MerchantIdentifier { get; set; }

        [JsonProperty("merchant_reference")]
        public string MerchantReference { get; set; }

        public string Amount { get; set; }

        public string Currency { get; set; }

        public string Language { get; set; }

        [JsonProperty("token_name")]
        public string TokenName { get; set; }

        public string Signature { get; set; }

        [JsonProperty("return_url")]
        public string ReturnUrl { get; set; }

        [JsonProperty("customer_email")]
        public string CustomerEmail { get; set; }

        //[JsonProperty("customer_ip")]
        //public string CustomerIp { get; set; }

        [JsonProperty("order_description")]
        public string OrderDescription { get; set; }

        //[JsonProperty("card_number")]
        //public string CardNumber { get; set; }

        //[JsonProperty("card_holder_name")]
        //public string Name { get; set; }

        //[JsonProperty("expiry_date")]
        //public string ExpiryDate { get; set; }

        //[JsonProperty("card_bin")]
        //public string cardbin { get; set; }
    }
}