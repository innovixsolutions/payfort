﻿using Newtonsoft.Json;
using PaymentProcessor.PayFort.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PaymentProcessor.PayFort.Model
{
    [JsonObject(NamingStrategyType = typeof(LowercaseNamingStrategy))]
    public class PurchaseResponse
    {
        public string Command { get; set; }

        [JsonProperty("access_code")]
        public string AccessCode { get; set; }

        [JsonProperty("merchant_identifier")]
        public string MerchantIdentifier { get; set; }

        [JsonProperty("merchant_reference")]
        public string merchant_reference { get; set; }

        public string Amount { get; set; }

        public string Currency { get; set; }

        public string Language { get; set; }

        [JsonProperty("customer_email")]
        public string CustomerEmail { get; set; }

        [JsonProperty("token_name")]
        public string TokenName { get; set; }

        public string signature { get; set; }

        [JsonProperty("fort_id")]
        public string FortId { get; set; }

        [JsonProperty("payment_option")]
        public string PaymentOption { get; set; }

        [JsonProperty("response_code")]
        public string ResponseCode { get; set; }

        [JsonProperty("response_message")]
        public string ResponseMessage { get; set; }

        [JsonProperty("order_description")]
        public string OrderDescription { get; set; }

        public string Status { get; set; }

        [JsonProperty("settlement_reference")]
        public string settlement_reference { get; set; }

        [JsonProperty("3ds_url")]
        public string _3DSUrl { get; set; }
    }
}